# Sudoku Solver Project
'''
      ******************************************************
      author: @akurnya
      maintainer: @akurnya
    ******************************************************
'''
#Raise Error for over exceeding the #Cols & Rows
class Row_ColError(Exception):
    def __init__(self,row,col):
        super(type(self),self).__init__()
        self.row = row
        self.col = col
        self.row_val = self.row < 9
        self.col_val = self.col < 9
    
    def __str__(self):
        if self.row_val == self.col_val == False:
            return "Row {} & Col {} is more than allowed value".format\
            (repr(self.row),repr(self.col))
        elif not self.row_val:
            return "Row {} is more than allowed value".format\
            (repr(self.row))
        else:
            return "Col {} is more than allowed value".format\
            (repr(self.col))


def get_cell_block(row,col):
    if row < 9 and col < 9:
        #row / 3 - you get the block # by splitting row into 3 parts - 0,1,2 | row={0..8}
        #then 3 * (row/3) gets you the starting row of that block
        block_row = 3 * (row / 3)
        block_col = col / 3
        return block_row + block_col  #block = block_row + block_col
    raise Row_ColError(row,col)

def is_possible_row(sudoku, row, number):
    # This is_possible_row() function returns true if the 'number'
    # argument is NOT already in the row of the 'sudoku' indexed
    # by the 'row' argument . This would mean that adding the
    # 'number' to the 'row' for this 'sudoku' is a potentially legal move.
    # (The 'sudoku' argument is a 9x9 two-dimensional array.)
    # This function should return false if the number already exists in
    # the specified row.
    "REPLACE THIS CODE WITH YOUR is_possible_row() METHOD"
    #Scan all cols with row for number
    for col in xrange(9):
        if number == sudoku[row][col]:
            return False
    return True
    
def is_possible_column(sudoku, col, number) :
    # This is a lot like is_possible_row() above: it returns true if the 'number'
    # argument is NOT already in the column of the 'sudoku' indexed
    # by the 'col' argument . This would mean that adding the
    # 'number' to the 'col' for this 'sudoku' is a potentially legal move.
    # This function should return false if the number already exists in
    # the specified column.
    "REPLACE THIS CODE WITH YOUR is_possible_column() METHOD"
    #Scan all rows with col for number
    for row in xrange(9):
        if number == sudoku[row][col]:
            return False
    return True
    
def is_possible_block(sudoku, block, number) :
    # A 'block' is a 3x3 area of the 'sudoku' in which numbers 1 to 9
    # must all be present. There are 9 of these zones in the Sudoku.
    # This method will return true if the 'number' argument is not already
    # in the block of the 'sudoku' specified by the 'block' argument --
    # again indicating a potentially legal move. (The 'sudoku' argument is,
    # as always, a 9x9 two-dimensional array.)
    # This method should return false if the 'number' already exists in the
    # specified 'block'.
    # The blocks are indexed from 0 to 8. The top row blocks being 0 to 2,
    # the middle row blocks being from 3 to 5 and the bottom row being 6 to 8.
    # Btw, the get_cell_block() function below might be helpful here.
    " REPLACE THIS CODE WITH YOUR is_possible_block() METHOD"
    starting_block_row = 3 * (block/3)
    starting_block_col = (block % 3) * 3
    #Scan all rows with all cols in Range of block for number
    for r in xrange(starting_block_row, starting_block_row+3):
        for c in xrange(starting_block_col, starting_block_col+3):
            if sudoku[r][c] == number:
                return False
    return True
    
def is_possible_number(sudoku, row, col, number) :
    # Now you will start using of the other methods you've written above.
    # This method accepts a 'sudoku' puzzle, a 'row' and a 'col' (column), and a
    # possible 'number' to move into that row/column position.
    # Such a move would be possible if: 1) is_possible_row() returns true for
    # that row, and 2) ifPossibleColumn() returns true for that column, and
    # 3) is_possible_block() returns true for the block that sudoku[row][col] is
    # in. Use the get_cell_block() function below to find which block this is. If all of
    # these functions return true, then is_possible_number() should return true.
    # If any of the those functions returns false, then ifPossibleNumber() should
    # return false.
    "REPLACE THIS CODE WITH YOUR is_possible_number() METHOD"
    block = get_cell_block(row, col)
    if is_possible_row(sudoku, row, number)\
    and is_possible_column(sudoku, col, number)\
    and is_possible_block(sudoku, block, number): return True
    return False
    
def is_correct_row(sudoku, row) :
    # This method should return true if all the numbers from 1 to 9 are
    # present in the row indexed by the 'row' argument in the 'sudoku'
    # puzzle. It should return false if the row is incomplete or has duplicates.
    # Note that the "empty" rows will actually contain the value 0.
    # 1) Start off by making a *copy* of the row.
    # 2) Then sort the new array that you have made so that the
    # numbers run in ascending order.
    # 3) You should then be able to run through this sorted array and
    # easily determine if all the required numbers are there.
    "REPLACE THIS CODE WITH YOUR is_correct_row() METHOD"
    #if row contains numbers 1-9
    #if row contains unique number
    return True if len(set(sudoku[row].values())) == 9 else False

    
def is_correct_column(sudoku, col) :
    # This method should return true if all the numbers from 1 to 9
    # are present in the column indexed by the 'col' argument in the
    # 'sudoku' puzzle. It should return false if the column is incomplete
    # or has duplicates.
    # This method is analogous to the is_correct_row() method above.
    " REPLACE THIS CODE WITH YOUR is_correct_column() METHOD"
    #if col contains numbers 1-9
    #if col contains unique number
    return True if len({sudoku[r][col] for r in sudoku})==9 else False

def is_correct_block(sudoku, block):
    # This function is very similar to the previous two above except
    # that you will again have to figure out how to index all the elements
    # in the 'sudoku' array for the block specified by the 'block' argument.
    # This function should return true if all the numbers from 1 to 9 are
    # in the block. It should return false if the block is incomplete or
    # contains duplicates.
    "REPLACE THIS CODE WITH YOUR is_correct_block() METHOD"
    starting_block_row = 3 * (block/3)
    end_row = starting_block_row+3
    starting_block_col = (block % 3) * 3
    end_col = starting_block_col + 3
    return True if len({sudoku[r][c] for r in xrange(starting_block_row, end_row)
    for c in xrange(starting_block_col, end_col)}) == 9 else False
    
    
def is_solved(sudoku) :
    # A sudoku will be solved if all its rows, columns and blocks are correct.
    # Use a combination of the previous three functions -- is_correct_row(),
    # is_correct_column(), and is_correct_block() -- to determine if the 'sudoku'
    # is solved and return true if this is the case. If the sudoku is not solved,
    # you should return false.
    "REPLACE THIS CODE WITH YOUR is_solved() METHOD"
    for i in xrange(9):
        if not (is_correct_row(sudoku,i) and is_correct_block(sudoku, i) and is_correct_column(sudoku, i)): return False
    return True

def check_sudoku(sudoku):
    '''Checks a given block or whole sudoku for correctness'''
    list_of_lists = check_sudoku([each.values() for each in sudoku.values()])
    index = (0,0)
    N = len(list_of_lists[0])
    #O(N^3)
    for r in xrange(N):
        for c in xrange(N):
            index = (r,c)
            #check col wise
            for each_list_index in xrange(N):
                if (each_list_index, c) != index and \
                list_of_lists[each_list_index][c] == list_of_lists[r][c]:
                    return False
            #col row wise
            for each_col_index in xrange(N):
                if (r, each_col_index) != index and \
                list_of_lists[r][each_col_index] == list_of_lists[r][c]:
                    return False
    return True

"""
#TODOS:implement the following logic:
    Check for solved cells 	2
    Show Possibles	Yes/No By:
    1: Hidden Singles	 
    2: Naked Pairs/Triples	 
    3: Hidden Pairs/Triples	 
    4: Naked Quads	 
    5: Pointing Pairs	 
    6: Box/Line Reduction
"""

if __name__ == "__main__":
    sudoku = dict((r,dict((c,0) for c in xrange(9))) for r in xrange(9))
    print is_possible_number(sudoku, 5, 4, 8)